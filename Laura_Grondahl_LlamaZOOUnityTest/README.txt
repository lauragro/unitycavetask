This project has been created for evaluation by LlamaZOO Interactive.

Author: 	Laura Grondahl
Date: 		October 14, 2016

Summary:	This project creates cave systems with cellular automata
		for use in any Unity 2D project. The caves are
		customizable and may be saved as prefabs. If the cave system
		includes at least one cave (i.e. all cells are not walls)
		then a player is created within the caves, which may be 
		controlled with the arrow keys.
---------------------------------------------------------------------
To run from scratch:
	0)	Clone this repository to a local directory
	1)	Generate new Unity 2D Project
	2)	Import package - Custom package - CaveCreator.unitypackage
	3)	Create new empty game object
	4)	Add script component to game object: GenerateCaves.cs
	5)	Select preferences: width, height, sprites, number of steps 
		(or generations), and method of cellular automation
	6)	Press 'Generate'
	6)	Adjust Main Camera to view entire cave system
	7) 	PLAY!
	8)	Optional: Press 'Save Prefab' to save the current cave system 
		to the Assets directory

A nice example's parameters:
	Width		=	17
	Height		=	12
	Wall Sprite	=	Sprites/Wall.png
	Cave Sprite	=	Sprites/Cave.png
	Player Sprite	=	Sprites/Player.png
	Steps		=	27
	Method		=	Parity
