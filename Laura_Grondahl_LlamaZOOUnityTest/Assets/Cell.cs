﻿/*
 * This project has been created for evaluation by LlamaZOO Interactive.
 * This script defines the Cell object, which the cave system is comprised of.
 * 
 * Author:  Laura Grondahl
 * Date:    October 14, 2016
 */

using UnityEngine;
using System.Collections;

public enum States {wall, cave};


public class Cell {

    // Defines the type of cell
    private States state;
    private States nextState;

    // The neighbourhood of cells used in procedural steps
    private ArrayList neighbourhood;

    // The neighbourhood without corner cells; used to remove islands
    private ArrayList adjacencies;

    // The position of the cell
    public Vector3 coords;

    // Constructor
    public Cell()
    {
        this.state = States.wall;
        this.nextState = this.state;
        this.neighbourhood = new ArrayList();
        this.adjacencies = new ArrayList();
    }

    // Getters and setters
    public States GetState()
    {
        return this.state;
    }
    public void SetState(States newState)
    {
        this.state = newState;
    }
    public States GetNextState()
    {
        return this.nextState;
    }
    public void SetNextState(States newNextState)
    {
        this.nextState = newNextState;
    }
    public ArrayList GetNeighbourhood()
    {
        return this.neighbourhood;
    }
    public void SetNeighbourhood(ArrayList newNeighbourhood)
    {
        this.neighbourhood = newNeighbourhood;
    }
    public ArrayList GetAdjacencies()
    {
        return this.adjacencies;
    }
    public void SetAdjacencies(ArrayList newAdjacencies)
    {
        this.adjacencies = newAdjacencies;
    }

    // Add given cell to the neighbourhood of this cell
    public void AddToNeighbourhood(Cell newNeighbour)
    {
        this.neighbourhood.Add(newNeighbour);
    }

    // Add given cell to the adjacencies list of this cell
    public void AddToAdjacencies(Cell newAdjacency)
    {
        this.adjacencies.Add(newAdjacency);
    }
}
