﻿/*
 * This project has been created for evaluation by LlamaZOO Interactive.
 * This is the main script for creating caves.
 * 
 * Author:  Laura Grondahl
 * Date:    October 14, 2016
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;   // For dictionary
using UnityEditor;  // For prefabs
using System;

public enum Procedure {toggle, parity};

public class GenerateCaves : MonoBehaviour {

    // Input parameters
    public int width;
    public int height;
    public Sprite wallSprite;
    public Sprite caveSprite;
    public Sprite playerSprite;
    public int steps;
    public Procedure method;

    // Private attributes
    private Cell[,] cells;
    private static string worldName = "World";     
    private static string playerName = "Player";

    // Called from GenerateCavesEditor script
    public void Generate()
    {
        // Create null grid and associated world and player objects
        cells = new Cell[width,height];

        // Initialize cells as walls
        for (int row=0; row<height; row++)
        {
            for(int col=0; col<width; col++)
            {
                cells[col,row] = new Cell();
                cells[col, row].coords = new Vector3(col, row, 0f);
            }
        }

        // Determine neighbourhoods (need separate loop as Cells must be initialied first)
        Cell currentCell;
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                // Store current cell
                currentCell = cells[col, row];

                // Add to own neighbourhood
                currentCell.AddToNeighbourhood(currentCell);

                // Add to all adjacent neighbourhoods
                // Leftmost column
                if(col-1 >= 0)
                {
                    cells[col - 1, row].AddToNeighbourhood(currentCell);
                    cells[col - 1, row].AddToAdjacencies(currentCell);
                    if(row-1 >= 0)
                        cells[col - 1, row - 1].AddToNeighbourhood(currentCell);  // Corner
                    if (row + 1 < height)
                        cells[col - 1, row + 1].AddToNeighbourhood(currentCell);    // Corner
                        
                }

                // Rightmost column                
                if (col+1 < width)
                {
                    cells[col + 1, row].AddToNeighbourhood(currentCell);
                    cells[col + 1, row].AddToAdjacencies(currentCell);
                    if (row - 1 >= 0)
                        cells[col + 1, row - 1].AddToNeighbourhood(currentCell);  // Corner
                    if (row + 1 < height)
                        cells[col + 1, row + 1].AddToNeighbourhood(currentCell);  // Corner
                        
                }

                // Centre column
                if (row - 1 >= 0)
                {
                    cells[col, row - 1].AddToNeighbourhood(currentCell);
                    cells[col, row - 1].AddToAdjacencies(currentCell);
                }
                if (row + 1 < height)
                {
                    cells[col, row + 1].AddToNeighbourhood(currentCell);
                    cells[col, row + 1].AddToAdjacencies(currentCell);
                }
            }

        }

        // Start centre cell as cave
        cells[width / 2, height / 2].SetState(States.cave);

        // Run procedural generation
        if(method == Procedure.toggle)
        {
            // Simplest
            ProcedureToggle();  
        }
        else if(method == Procedure.parity)
        {
            // Next simplest, but requires post-process to remove 'island caves'
            ProcedureParity();  
            RemoveIslands(); 
        }
            
        
        // Create game objects and draw the caves
        CreateObjects();

        // Create player positioned within cave, if a cave exists
        Transform worldTransform = transform.Find(worldName);
        if(worldTransform != null)
        {
            int numChildren = worldTransform.childCount;
            if (numChildren > 0)
            {
                bool caveFound = false;
                for (int row = 0; row < height; row++)
                {
                    for (int col = 0; col < width; col++)
                    {
                        if (cells[col, row].GetState() == States.cave)
                        {
                            // Set player z coordinate so player is visible
                            CreatePlayer(cells[col, row].coords - new Vector3(0,0,1));
                            caveFound = true;
                            break;

                        }

                        // Only create one player
                        if (caveFound)
                            break;

                    }

                }

            }
            
        }
        

    }


    // Called from GenerateCavesEditor script
    public void Save()
    {
        // Compose path
        string path = "Assets/CaveSystem.prefab";

        // Save with unique name
        PrefabUtility.CreatePrefab(AssetDatabase.GenerateUniqueAssetPath(path), gameObject);
    }

    private object Load(string v)
    {
        throw new NotImplementedException();
    }


    // Simplest first: switch cell states at each step
    private void ProcedureToggle()
    {
        int step = 0;
        while(step < steps)
        {
            
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    // Toggle state
                    if(cells[col,row].GetState() == States.wall)
                    {
                        cells[col, row].SetState(States.cave);
                    }
                    else
                    {
                        cells[col, row].SetState(States.wall);
                    }
                }
            }

            step++;
        }

    }


    // More involved procedure: set state according to neighbourhood parity
    private void ProcedureParity()
    {
        int step = 0;
        while (step < steps)
        {
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    // Edge cases
                    if (cells[col, row].GetNeighbourhood().Count < 9)
                    {
                        cells[col, row].SetNextState(States.wall);
                        continue;
                    }
                        
                    // Determine neighbourhood parity
                    int parity = CountCaves(cells[col, row].GetNeighbourhood()) % 2;

                    // Set next state accordingly
                    if (parity > 0)
                    {
                        cells[col, row].SetNextState(States.wall);
                    }
                    else
                    {
                        cells[col, row].SetNextState(States.cave);
                    }
                }

            }

            step++;

            // Apply changes to cells grid
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    cells[col, row].SetState(cells[col, row].GetNextState());
                }
            }

        } //endwhile

    }


    // Post-process: remove 'island caves'
    private void RemoveIslands()
    {
        Cell firstCave = new Cell();

        // Find cave to start traversal
        bool found = false;
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                if (cells[col, row].GetState() == States.cave)
                {
                    firstCave = cells[col, row];
                    found = true;

                    // Exit nested loop
                    break;
                }
            }

            // Exit outer loop
            if (found)
                break;
        }

        // No caves found so nothing to do
        if (!found)
            return;

        // Setup structures for traversing cells (Dictionary keys matter, but values are just placeholders)
        Dictionary<Cell, bool> visited = new Dictionary<Cell, bool>();
        Queue<Cell> caves = new Queue<Cell>();

        // Add first cave to each structure
        visited.Add(firstCave, false);
        caves.Enqueue(firstCave);

        // Traverse through all connected caves and mark as visited
        while (caves.Count > 0)
        {
            Cell cave = caves.Dequeue();

            foreach (Cell adjacency in cave.GetAdjacencies())
            {
                // If we found a new cave
                if (adjacency.GetState() == States.cave && !visited.ContainsKey(adjacency))
                {
                    visited.Add(adjacency, false);
                    caves.Enqueue(adjacency);
                }
            }
        }

        // Revert all unvisited caves to walls
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                if (cells[col,row].GetState() == States.cave && !visited.ContainsKey(cells[col, row]))
                {
                    cells[col, row].SetState(States.wall);
                }
            }
        }

    }


    // Given an array of states, return the number of caves
    private int CountCaves(ArrayList neighbourhood)
    {
        int numCaves = 0;

        foreach (Cell item in neighbourhood)
        {
            if (item.GetState() == States.cave)
                numCaves++;
        }  

        return numCaves;
    }


    // Create objects to render cells
    private void CreateObjects()
    {
        // Clear pre-existing world 
        GameObject oldWorld = GameObject.Find(worldName);
        if (oldWorld != null)
        {
            DestroyImmediate(oldWorld);
        }

        // Create new world
        GameObject world = new GameObject(worldName);
        world.transform.SetParent(transform);
                
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                // Create object with sprite renderer for cell in Unity scene
                GameObject cellObject = new GameObject("cell_" + col + "_" + row);
                cellObject.transform.position = cells[col, row].coords;
                SpriteRenderer renderer = cellObject.AddComponent<SpriteRenderer>();

                // Determine sprite based on cell state
                if (cells[col, row].GetState() == States.wall)
                {
                    renderer.sprite = wallSprite;

                    // Add collisions for walls
                    cellObject.AddComponent<BoxCollider2D>();
                }
                else
                {
                    renderer.sprite = caveSprite;
                }

                // Set cell as child to the game object calling this script
                cellObject.transform.SetParent(world.transform);
            }
            
        }
        
    }


    // Create a player
    public void CreatePlayer(Vector3 position)
    {
        // Clear pre-existing player
        GameObject oldPlayer = GameObject.Find(playerName);
        if (oldPlayer != null)
        {
            DestroyImmediate(oldPlayer);
        }

        // Create new player
        GameObject player = new GameObject(playerName);
        player.transform.SetParent(transform);

        // Add sprite to view player
        SpriteRenderer renderer = player.AddComponent<SpriteRenderer>();
        renderer.sprite = playerSprite;

        // Add collider for collisions with walls
        player.AddComponent<CircleCollider2D>();

        // Add script for controlling player
        player.AddComponent<PlayerControllerScript>();

        // Add ridid body for moving player
        Rigidbody2D rigidBody = player.AddComponent<Rigidbody2D>();
        rigidBody.useAutoMass = true;
        rigidBody.gravityScale = 0f;    // For top-down view, ignore gravity
        rigidBody.collisionDetectionMode = CollisionDetectionMode2D.Continuous; // For a smooth animation

        // move player to within cave, with z comp set infront of caves
        player.transform.position = position;


    }

}
