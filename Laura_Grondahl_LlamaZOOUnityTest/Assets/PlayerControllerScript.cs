﻿/*
 * This project has been created for evaluation by LlamaZOO Interactive.
 * This is the script for controlling the player object.
 * 
 * Author:  Laura Grondahl
 * Date:    October 14, 2016
 */

using UnityEngine;
using System.Collections;

public class PlayerControllerScript : MonoBehaviour {

    public float maxSpeed = 10f;

	// Use this for initialization
	void Start () {

	}
	

	// Update is called once per frame
	void Update () {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        transform.position += move * maxSpeed * Time.deltaTime;
    }
}
