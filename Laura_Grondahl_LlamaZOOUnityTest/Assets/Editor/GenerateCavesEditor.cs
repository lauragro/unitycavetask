﻿/*
 * This project has been created for evaluation by LlamaZOO Interactive.
 * This is the script for setting up the editor window for generating caves.
 * 
 * Author:  Laura Grondahl
 * Date:    October 14, 2016
 */

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(GenerateCaves))]
public class GenerateCavesEditor : Editor {

	// When Inspector opens
    public override void OnInspectorGUI()
    {
        // Include defaults for inspector window
        DrawDefaultInspector();

        // Capture overbearing script
        GenerateCaves script = (GenerateCaves)target;

        // Add button for generating caves
        if (GUILayout.Button("Generate"))
        {
            // Run the script when the button is pressed
            script.Generate();
        }

        // Add button for saving caves as prefabs
        if (GUILayout.Button("Save Prefab"))
        {
            // Run the script when the button is pressed
            script.Save();
        }
    }

}
